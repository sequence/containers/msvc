# MSVC Container

Dotnet SDK on windows server core LTSC2019 that has
[Visual C++ runtime](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0)
installed.

# Build and Push

```
docker build -t registry.gitlab.com/sequence/containers/msvc .
docker push registry.gitlab.com/sequence/containers/msvc
```
