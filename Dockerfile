FROM mcr.microsoft.com/dotnet/sdk:6.0-windowsservercore-ltsc2019

WORKDIR /temp

ADD https://aka.ms/vs/16/release/vc_redist.x64.exe vc_redist.x64.exe

RUN vc_redist.x64.exe /install /quiet /norestart /log "vc_redist.log"

WORKDIR /

RUN rmdir /Q /S temp
